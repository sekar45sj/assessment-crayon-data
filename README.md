# Overview

```
Build a AWS Infrastructure using Terraform Scripts.
Deploy Postgres and Apache2 Server in Docker Container inside EC2 instances.
Creating Load Balancer and mapping Apache2 Server's.
```



# Setup Process

#### 1. Create EC2 Instance  
```
Create EC2 Instance for executing script to build Infrastructure.
AMI : Ubuntu 18.04 [setup.sh script written for ubuntu machine]
```
 
#### 2. Attach IAM Role or Configure AWS using Access Key and Secret Key
```
Create a Role with EC2 and VPC access. Attach the role to EC2 Instance.
                                  [or]  
Store AWS Access Key & Secret Key in EC2 instance using aws configure.
```

#### 3. Clone Git Repository
```
Clone the Git Repository.

COMMAND :
git clone https://gitlab.com/sekar45sj/assessment-crayon-data.git
```

#### 4. Execute setup.sh script
```  
Execute the setup.sh

COMMAND :
cd assessment-crayon-data
bash setup.sh

It perform below operations.

1. Installing Terraform
2. Postgres DB Password Creation
3. Initializing Terraform
4. Planning Terraform
5. Applying Terraform
6. Pem File Creation

While applying terraform, it will create below aws resources 
* 1 VPC [xyz-vpc]
* 2 Subnets [xyz-app-subnet & xyz-db-subnet]
* 1 Internet Gateway [xyz-internet-gateway]
* 1 NAT Gateway [xyz-nat-gateway]
* 2 Route Table  [xyz-app-subnet-route-table & xyz-db-subnet-route-table]
* 3 Security Group [xyz-app-subnet-nsg, xyz-db-subnet-nsg & xyz-lb-nsg]
* 1 Key Pair [xyz-key-pair]
* 2 Instances in xyz-app-subnet with Apache2 Server dockerized
* 2 Instances in xyz-db-subnet with Postgresql DB Server dockerized
* 1 Load Balancer with target as instances in xyz-app-subnet [xyz-load-balancer]
```

#### 5. Accessing App Server & DB Server  
```
After executing setup script, you will get below details.
* load balancer dns name
* app server public and private ip's
* db server private ip
* postgres master password
* pem file created with name xyz-key.pem

By using load balancer dns name in browser, can access App servers.
By using pem, can login into the app server ec2 instance. From there can connect to postgres db.

COMMAND :
psql -U postgres -h <db-server-private-ip>  [ It will prompt for postgres password ]
```



# Destroy Process

#### 1. Destroy Terraform
```  
Destroy the aws resources by destroying terraform.

COMMAND :
cd assessment-crayon-data/terraform-scripts
terraform destroy --auto-approve
```

