#!/bin/bash

# Install Terraform
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform -y

# Setting Postgres Password
cd terraform-scripts
cp template-install-db.sh install-db.sh
pg_password=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 12)
sed -i "s/REPLACE_POSTGRES_PASSWORD/$pg_password/g" install-db.sh

# Initialize Terraform Working Directory
terraform init
if [ $? -ne 0 ]; then
  echo "Terraform Initialization Failed.."
  exit 1
fi

# Generating Terraform Plan
terraform plan
if [ $? -ne 0 ]; then
  echo "Terraform Plan Failed.."
  exit 1
fi

# Applying Terraform
terraform apply --auto-approve
if [ $? -ne 0 ]; then
  echo "Terraform Exeution Failed.."
  exit 1
fi

# Printing Postgres Master Password
echo "Postgres Master Password for user postgres is $pg_password"

# Creating pem file
terraform output key_pair > ../xyz-key.pem
chmod 400 ../xyz-key.pem
echo "Pem file created with name xyz-key.pem"
