/*
    It is the main file which calls modules and create services.
    Define the region, where to build a infrastructure.
*/
provider "aws" {
  region = var.REGION
}



/*
    It calls VPC module which is mentioned in the source path to create a VPC. 
*/
module "vpc" {
  source         = "./network/vpc"
  VPC_CIDR_BLOCK = var.VPC_CIDR_BLOCK
  VPC_TAG_NAME   = var.VPC_TAG_NAME
}



/*
    It calls Subnet module which is mentioned in the source path to create a Subnets.
*/
module "subnets" {
  source                            = "./network/subnets"
  VPC_ID                            = module.vpc.vpc_id
  PRIVATE_SUBNET_AVAILABILITY_ZONE  = var.PRIVATE_SUBNET_AVAILABILITY_ZONE
  PRIVATE_SUBNET_CIDR_BLOCK         = var.PRIVATE_SUBNET_CIDR_BLOCK
  PRIVATE_SUBNET_TAG_NAME           = var.PRIVATE_SUBNET_TAG_NAME
  PUBLIC_SUBNET_AVAILABILITY_ZONE   = var.PUBLIC_SUBNET_AVAILABILITY_ZONE
  PUBLIC_SUBNET_CIDR_BLOCK          = var.PUBLIC_SUBNET_CIDR_BLOCK
  PUBLIC_SUBNET_TAG_NAME            = var.PUBLIC_SUBNET_TAG_NAME
}



/*
    It calls Route module which is mentioned in the source path to create a Routes.
*/
module "internet-gateway" {
  source                    = "./network/internet-gateway"
  VPC_ID                    = module.vpc.vpc_id
  INTERNET_GATEWAY_TAG_NAME = var.INTERNET_GATEWAY_TAG_NAME
}



/*
    It calls Route module which is mentioned in the source path to create a Routes.
*/
module "nat-gateway" {
  source                = "./network/nat-gateway"
  PUBLIC_SUBNET_ID      = module.subnets.public_subnet_id
  NAT_GATEWAY_TAG_NAME  = var.NAT_GATEWAY_TAG_NAME
}



/*
    It calls Route module which is mentioned in the source path to create a Routes.
*/
module "routes" {
  source                              = "./network/route"
  VPC_ID                              = module.vpc.vpc_id
  PUBLIC_SUBNET_ID                    = module.subnets.public_subnet_id
  INTERNET_GATEWAY_ID                 = module.internet-gateway.internet_gateway_id
  PUBLIC_SUBNET_ROUTE_TABLE_TAG_NAME  = var.PUBLIC_SUBNET_ROUTE_TABLE_TAG_NAME
  PRIVATE_SUBNET_ID                   = module.subnets.private_subnet_id
  NAT_GATEWAY_ID                      = module.nat-gateway.nat_gateway_id
  PRIVATE_SUBNET_ROUTE_TABLE_TAG_NAME = var.PRIVATE_SUBNET_ROUTE_TABLE_TAG_NAME
  EGRESS_CIDR_BLOCK                   = var.EGRESS_CIDR_BLOCK
}



/*
    It calls Route module which is mentioned in the source path to create a Routes.
*/
module "security-groups" {
  source                      = "./network/security-groups"
  VPC_ID                      = module.vpc.vpc_id 
  DB_SECURITY_GROUP_TAG_NAME  = var.DB_SECURITY_GROUP_TAG_NAME
  DB_PORT                     = var.DB_PORT
  DB_PROTOCOL                 = var.DB_PROTOCOL
  PUBLIC_SUBNET_CIDR_BLOCK    = var.PUBLIC_SUBNET_CIDR_BLOCK
  APP_SECURITY_GROUP_TAG_NAME = var.APP_SECURITY_GROUP_TAG_NAME
  APP_PORT                    = var.APP_PORT
  APP_PROTOCOL                = var.APP_PROTOCOL
  LB_SECURITY_GROUP_TAG_NAME  = var.LB_SECURITY_GROUP_TAG_NAME
  EGRESS_CIDR_BLOCK           = var.EGRESS_CIDR_BLOCK
  SSH_PORT                    = var.SSH_PORT
  SSH_PROTOCOL                = var.SSH_PROTOCOL
}



/*
    It calls EC2 module which is mentioned in the source path to create a EC2 instance.
*/
module "instance" {
  source                = "./ec2/instance"
  VPC_ID                = module.vpc.vpc_id
  PUBLIC_SUBNET_ID      = module.subnets.public_subnet_id
  PRIVATE_SUBNET_ID     = module.subnets.private_subnet_id
  AMI_PREFIX            = var.AMI_PREFIX
  APP_INSTANES_LIST     = var.APP_INSTANES_LIST
  DB_INSTANES_LIST      = var.DB_INSTANES_LIST
  APP_SECURITY_GROUP_ID = module.security-groups.app_security_group_id
  DB_SECURITY_GROUP_ID  = module.security-groups.db_security_group_id
  KEY_PAIR_NAME         = var.KEY_PAIR_NAME
}



/*
    It calls Load Balancer module which is mentioned in the source path to create a Load Balancer.
*/
module "load-balancer" {
  source                          = "./ec2/load-balancer"
  ELB_TAG_NAME			  = var.ELB_TAG_NAME
  PUBLIC_SUBNET_ID                = module.subnets.public_subnet_id
  ELB_PORT                        = var.ELB_PORT
  ELB_PROTOCOL                    = var.ELB_PROTOCOL
  APP_PORT                        = var.APP_PORT
  APP_PROTOCOL                    = var.APP_PROTOCOL
  APP_INSTANCES_ID                = module.instance.app_instance_id
  LB_SECURITY_GROUP_ID            = module.security-groups.lb_security_group_id
}


