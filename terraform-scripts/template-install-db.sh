#!/bin/bash

# Install Docker
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# Run Docker Conatainer for Postgres
docker run -itd --name postgres -e POSTGRES_PASSWORD=REPLACE_POSTGRES_PASSWORD -p 5432:5432 -v /opt/postgresql/data:/var/lib/postgresql/data postgres:12
