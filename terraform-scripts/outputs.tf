output "load_balancer_dns_name" {
  value = module.load-balancer.load_balancer_dns_name
}

output "app_instance_public_ip" {
  value = module.instance.app_instance_public_ip
}

output "app_instance_private_ip" {
  value = module.instance.app_instance_private_ip
}

output "db_instance_private_ip" {
  value = module.instance.db_instance_private_ip
}

output "key_pair" {
  value = module.instance.key_pair
}

