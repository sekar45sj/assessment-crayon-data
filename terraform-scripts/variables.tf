/*
  Variables for AWS Region
*/
variable "REGION" {
  default     = "ap-south-1"
  description = "AWS Region"
}



/*
  Variables for Network resource VPC
*/
variable "VPC_CIDR_BLOCK" {
  default     = "10.1.0.0/24"
  description = "CIDR block of the VPC"
}

variable "VPC_TAG_NAME" {
  default     = "xyz-vpc"
  description = "Tags to be added to the VPC"
}



/*
  Variables for Network resource Subnet
*/
variable "PUBLIC_SUBNET_AVAILABILITY_ZONE" {
  default     = "ap-south-1a"
  description = "Public Subnet Availability Zone"
}

variable "PUBLIC_SUBNET_CIDR_BLOCK" {
  default     = "10.1.0.0/27" 
  description = "Public Subnet CIDR Range"
}

variable "PUBLIC_SUBNET_TAG_NAME" {
  default     = "xyz-app-subnet"
  description = "Public Subnet Tag Name"
}
variable "PRIVATE_SUBNET_AVAILABILITY_ZONE" {
  default     = "ap-south-1b"
  description = "Private Subnet Availability Zone"
}

variable "PRIVATE_SUBNET_CIDR_BLOCK" {
  default     = "10.1.0.32/27"
  description = "Private Subnet CIDR Range"
}

variable "PRIVATE_SUBNET_TAG_NAME" {
  default     = "xyz-db-subnet"
  description = "Private Subnet Tag Name"
}



/*
  Variables for Network resource Internet Gateway
*/
variable "INTERNET_GATEWAY_TAG_NAME" {
  default     = "xyz-internet-gateway"
  description = "Internet Gateway Tag Name"
}



/*
  Variables for Network resource NAT Gateway
*/
variable "NAT_GATEWAY_TAG_NAME" {
  default     = "xyz-nat-gateway"
  description = "NAT Gateway Tag Name"
}



/*
  Variables for Network resource Route Table
*/
variable "PUBLIC_SUBNET_ROUTE_TABLE_TAG_NAME" {
  default     = "xyz-app-subnet-route-table"
  description = "Public Subnet Route Table Tag Name"
}

variable "PRIVATE_SUBNET_ROUTE_TABLE_TAG_NAME" {
  default     = "xyz-db-subnet-route-table"
  description = "Private Subnet Route Table Tag Name"
}

variable "EGRESS_CIDR_BLOCK" {
  default     = "0.0.0.0/0"
  description = "Out Bound CIDR Block Range"
}



/*
  Variables for Network resource Security Group
*/
variable "DB_SECURITY_GROUP_TAG_NAME" {
  default     = "xyz-db-subnet-nsg"
  description = "DB Security Group Tag Name"
}

variable "DB_PORT" {
  default     = "5432"
  description = "DB Port"
}

variable "DB_PROTOCOL" {
  default     = "tcp"
  description = "DB Protocol"
}

variable "APP_SECURITY_GROUP_TAG_NAME" {
  default     = "xyz-app-subnet-nsg"
  description = "Application Security Group Tag Name"
}

variable "APP_PORT" {
  default     = "80"
  description = "Application Port"
}

variable "APP_PROTOCOL" {
  default     = "tcp"
  description = "Application Protocol"
}

variable "LB_SECURITY_GROUP_TAG_NAME" {
  default     = "xyz-lb-nsg"
  description = "Load Balancer Security Group Tag Name"
}

variable "SSH_PORT" {
  default     = 22
  description = "SSH Port"
}

variable "SSH_PROTOCOL" {
  default     = "TCP"
  description = "SSH Protocol"
}



/*
  Variables for EC2 instances
*/
variable "AMI_PREFIX" {
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"
  description = "Prefix of the AMI Image"
}

variable "KEY_PAIR_NAME" {
  default     = "xyz-key-pair"
  description = "Key Pair Name"
}

variable "APP_INSTANES_LIST" {
  default     = [ 
    {
      "instance_type": "t3.micro",
      "tag_name": "xyz-app-subnet-instance-1"
      "volume_size": "30"
    },
    {
      "instance_type": "t3.micro",
      "tag_name": "xyz-app-subnet-instance-2"
      "volume_size": "30"
    }
  ]
  description = "List of App Instances details"
}

variable "DB_INSTANES_LIST" {
  default     = [
    {
      "instance_type": "t3.micro",
      "tag_name": "xyz-db-subnet-instance-1"
      "volume_size": "100"
    },
    {
      "instance_type": "t3.micro",
      "tag_name": "xyz-db-subnet-instance-2"
      "volume_size": "100"
    }
  ]
  description = "List of DB Instances details"
}



/*
  Variables for Load Balancer
*/
variable "ELB_TAG_NAME" {
  default     = "xyz-load-balancer"
  description = "Elastic Load Balancer Tag Name"
}

variable "ELB_PORT" {
  default     = "80"
  description = "Elastic Load Balancer Port"
}

variable "ELB_PROTOCOL" {
  default     = "HTTP"
  description = "Elastic Load Balancer Protocol"
}


