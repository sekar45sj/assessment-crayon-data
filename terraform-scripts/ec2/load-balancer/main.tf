/*
    Load Balancer Creation
*/
resource "aws_elb" "elastic_load_balancer" {
  name               = var.ELB_TAG_NAME
  subnets            = [ var.PUBLIC_SUBNET_ID ]
  instances          = var.APP_INSTANCES_ID
  security_groups    = [ var.LB_SECURITY_GROUP_ID ]

  listener {
    lb_port           = var.ELB_PORT
    lb_protocol       = var.ELB_PROTOCOL
    instance_port     = var.ELB_PORT
    instance_protocol = var.ELB_PROTOCOL
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = format("%s:%s/", var.ELB_PROTOCOL, var.ELB_PORT)
    interval            = 30
  }

  tags = {
    Name = var.ELB_TAG_NAME
  }
}
