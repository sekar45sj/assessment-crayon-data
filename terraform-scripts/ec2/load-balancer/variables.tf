variable "ELB_TAG_NAME" {
  description = "Elastic Load Balancer Tag Name"
}

variable "PUBLIC_SUBNET_ID" {
  description = "Public Subnet ID"
}

variable "ELB_PORT" {
  description = "Elastic Load Balancer Port"
}

variable "ELB_PROTOCOL" {
  description = "Elastic Load Balancer Protocol"
}

variable "APP_PORT" {
  description = "Application Instance Port"
}

variable "APP_PROTOCOL" {
  description = "Application Instance Protocol"
}

variable "APP_INSTANCES_ID" {
  description = "Load Balancer Target Instances"
}

variable "LB_SECURITY_GROUP_ID" {
  description = "Load Balancer Security Group ID"
}

