/*
  Key creation for AWS Key Pair
*/
resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


/*
  AWS Key Pair Creation for ssh into the instance
*/
resource "aws_key_pair" "key_pair" {
  key_name   = var.KEY_PAIR_NAME
  public_key = tls_private_key.key.public_key_openssh
}


/*
  Define AWS AMI for instances
*/
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = [ var.AMI_PREFIX ]
  }

  filter {
    name   = "virtualization-type"
    values = [ "hvm" ]
  }

  owners = ["099720109477"] # Canonical
}


/*
  Creation of Application Instances in Public Subnet
*/
resource "aws_instance" "app_instance" {
  count                  = length(var.APP_INSTANES_LIST)
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.APP_INSTANES_LIST[count.index].instance_type
  subnet_id              = var.PUBLIC_SUBNET_ID
  vpc_security_group_ids = [ var.APP_SECURITY_GROUP_ID ]
  key_name               = aws_key_pair.key_pair.key_name

  root_block_device {
    volume_size = var.APP_INSTANES_LIST[count.index].volume_size
  }

  tags = {
    Name = var.APP_INSTANES_LIST[count.index].tag_name
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.key.private_key_pem
    host        = self.public_ip
  } 

  provisioner "file" {
    source      = "install-app.sh"
    destination = "/home/ubuntu/install-app.sh"
  }

  provisioner "file" {
    source      = "index.html"
    destination = "/home/ubuntu/index.html"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ubuntu/install-app.sh",
      "/home/ubuntu/install-app.sh"
    ]
  }
}


/*
  Creation of DB Instances in Private Subnet
*/
resource "aws_instance" "db_instance" {
  count                  = length(var.DB_INSTANES_LIST)
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.DB_INSTANES_LIST[count.index].instance_type
  subnet_id              = var.PRIVATE_SUBNET_ID
  vpc_security_group_ids = [ var.DB_SECURITY_GROUP_ID ]
  key_name               = aws_key_pair.key_pair.key_name
  user_data              = file("install-db.sh")

  root_block_device {
    volume_size = var.DB_INSTANES_LIST[count.index].volume_size
  }

  tags = {
    Name = var.DB_INSTANES_LIST[count.index].tag_name
  }
}
