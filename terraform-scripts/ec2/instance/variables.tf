variable "VPC_ID" {
  description = "VPC ID"
}

variable "PRIVATE_SUBNET_ID" {
  description = "Private Subnet ID"
}

variable "PUBLIC_SUBNET_ID" {
  description = "Public Subnet ID"
}

variable "AMI_PREFIX" {
  description = "Prefix of the AMI Image"
}

variable "APP_INSTANES_LIST" {
  description = "List of App Instances details"
}

variable "DB_INSTANES_LIST" {
  description = "List of DB Instances details"
}

variable "APP_SECURITY_GROUP_ID" {
  description = "App Security Group ID"
}

variable "DB_SECURITY_GROUP_ID" {
  description = "DB Security Group ID"
}

variable "KEY_PAIR_NAME" {
  description = "Key Pair Name"
}
