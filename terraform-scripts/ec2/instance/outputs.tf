output "app_instance_id" {
  value = aws_instance.app_instance.*.id
}

output "db_instance_id" {
  value = aws_instance.db_instance.*.id
}

output "key_pair" {
  value = tls_private_key.key.private_key_pem
}

output "app_instance_public_ip" {
  value = aws_instance.app_instance.*.public_ip
}

output "app_instance_private_ip" {
  value = aws_instance.app_instance.*.private_ip
}

output "db_instance_private_ip" {
  value = aws_instance.db_instance.*.private_ip
}

