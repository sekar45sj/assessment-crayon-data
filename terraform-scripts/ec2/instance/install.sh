#!/bin/bash

sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo apt install apache2 -y

instance_id=$(curl http://169.254.169.254/latest/meta-data/instance-id)
sed -i "s/REPLACE_INSTANCE_ID/$instance_id/g" index.html

mkdir /opt/apache2
cp mv index.html /opt/apache2/

docker run -itd -p 80:80 -v /opt/apache2:/usr/local/apache2/htdocs httpd

