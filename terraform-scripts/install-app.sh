#!/bin/bash

# Install Docker
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# Install Postgres Client
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-client-12 -y

# Modify Apahe Server Index HTML
instance_id=$(curl http://169.254.169.254/latest/meta-data/instance-id)
sed -i "s/REPLACE_INSTANCE_ID/$instance_id/g" index.html

sudo mkdir /opt/apache2
sudo mv index.html /opt/apache2/

# Run Docker Container for Apache Server
sudo docker run -itd -p 80:80 -v /opt/apache2:/usr/local/apache2/htdocs httpd

