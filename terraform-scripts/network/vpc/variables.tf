variable "VPC_CIDR_BLOCK" {
  description = "VPC CIDR Block Range"
}

variable "VPC_TAG_NAME" {
  description = "VPC Tag Name"
}
