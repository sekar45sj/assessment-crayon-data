/*
    Route Table Creation for Public Subnet
*/
resource "aws_route_table" "public_subnet_route_table" {
  vpc_id = var.VPC_ID
  route {
    cidr_block = var.EGRESS_CIDR_BLOCK
    gateway_id = var.INTERNET_GATEWAY_ID
  }
  tags = {
    Name = var.PUBLIC_SUBNET_ROUTE_TABLE_TAG_NAME
  }
}

/*
    Associate Public Subnet with Route Table
*/
resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = var.PUBLIC_SUBNET_ID
  route_table_id = aws_route_table.public_subnet_route_table.id
}

/*
    Route Table Creation for Private Subnet
*/
resource "aws_route_table" "private_subnet_route_table" {
  vpc_id = var.VPC_ID
  route {
    cidr_block = var.EGRESS_CIDR_BLOCK
    nat_gateway_id = var.NAT_GATEWAY_ID
  }
  tags = {
    Name = var.PRIVATE_SUBNET_ROUTE_TABLE_TAG_NAME
  }
}

/*
    Associate Private Subnet with Route Table
*/
resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = var.PRIVATE_SUBNET_ID
  route_table_id = aws_route_table.private_subnet_route_table.id
}
