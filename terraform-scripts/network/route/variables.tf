variable "VPC_ID" {
  description = "VPC ID for where the route table have to be created"
}

variable "PUBLIC_SUBNET_ID" {
  description = "Public Subnet ID"
}

variable "INTERNET_GATEWAY_ID" {
  description = "Internet Gateway ID"
}

variable "PUBLIC_SUBNET_ROUTE_TABLE_TAG_NAME" {
  description = "Public Subnet Route Table Tag Name"
}

variable "PRIVATE_SUBNET_ID" {
  description = "Private Subnet ID"
}

variable "NAT_GATEWAY_ID" {
  description = "NAT Gateway ID"
}

variable "PRIVATE_SUBNET_ROUTE_TABLE_TAG_NAME" {
  description = "Private Subnet Route Table Tag Name"
}

variable "EGRESS_CIDR_BLOCK" {
  description = "Out Bound CIDR Block Range"
}
