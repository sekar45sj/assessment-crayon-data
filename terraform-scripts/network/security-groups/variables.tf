variable "VPC_ID" {
  description = "VPC ID for where security groups have to be created"
}

variable "DB_SECURITY_GROUP_TAG_NAME" {
  description = "DB Security Group Tag Name"
}

variable "DB_PORT" {
  description = "DB Port"
}

variable "DB_PROTOCOL" {
  description = "DB Protocol"
}

variable "PUBLIC_SUBNET_CIDR_BLOCK" {
  description = "Public Subnet CIDR Block"
}

variable "APP_SECURITY_GROUP_TAG_NAME" {
  description = "Application Security Group Tag Name"
}

variable "APP_PORT" {
  description = "Application Port"
}

variable "APP_PROTOCOL" {
  description = "Application Protocol"
}

variable "LB_SECURITY_GROUP_TAG_NAME" {
  description = "Load Balancer Security Group Tag Name"
}

variable "EGRESS_CIDR_BLOCK" {
  description = "Out Bound CIDR Block Range"
}

variable "SSH_PORT" {
  description = "SSH Port"
}

variable "SSH_PROTOCOL" {
  description = "SSH Protocol"
}
