/*
    Security Group Creation for DB instances
*/
resource "aws_security_group" "db_security_group" {
  name        = var.DB_SECURITY_GROUP_TAG_NAME
  vpc_id      = var.VPC_ID

  ingress {
    from_port       = var.DB_PORT
    to_port         = var.DB_PORT
    protocol        = var.DB_PROTOCOL
    security_groups = [ aws_security_group.app_security_group.id ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [ var.EGRESS_CIDR_BLOCK ]
  }

  tags = {
    Name = var.DB_SECURITY_GROUP_TAG_NAME
  }
}

/*
    Security Group Creation for App instances 
*/
resource "aws_security_group" "app_security_group" {
  name        = var.APP_SECURITY_GROUP_TAG_NAME
  vpc_id      = var.VPC_ID

  ingress {
    from_port       = var.APP_PORT
    to_port         = var.APP_PORT
    protocol        = var.APP_PROTOCOL
    security_groups = [ aws_security_group.lb_security_group.id ]
  }

  ingress {
    from_port   = var.SSH_PORT
    to_port     = var.SSH_PORT
    protocol    = var.SSH_PROTOCOL
    cidr_blocks = [ var.EGRESS_CIDR_BLOCK ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [ var.EGRESS_CIDR_BLOCK ]
  }

  tags = {
    Name = var.APP_SECURITY_GROUP_TAG_NAME
  }
}

/*
    Security Group Creation for Load Balancer
*/
resource "aws_security_group" "lb_security_group" {
  name        = var.LB_SECURITY_GROUP_TAG_NAME
  vpc_id      = var.VPC_ID

  ingress {
    from_port   = var.APP_PORT
    to_port     = var.APP_PORT
    protocol    = var.APP_PROTOCOL
    cidr_blocks = [ var.EGRESS_CIDR_BLOCK ] 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [ var.EGRESS_CIDR_BLOCK ]
  }

  tags = {
    Name = var.LB_SECURITY_GROUP_TAG_NAME
  }
}

