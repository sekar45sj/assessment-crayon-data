/*
  Public Subnet Creation within specified VPC
*/
resource "aws_subnet" "public_subnet" {
  vpc_id                  = var.VPC_ID
  availability_zone       = var.PUBLIC_SUBNET_AVAILABILITY_ZONE
  cidr_block              = var.PUBLIC_SUBNET_CIDR_BLOCK
  map_public_ip_on_launch = "true"
  tags = {
    Name = var.PUBLIC_SUBNET_TAG_NAME
  }
}

/*
  Private Subnet Creation within specified VPC
*/
resource "aws_subnet" "private_subnet" {
  vpc_id                  = var.VPC_ID
  availability_zone       = var.PRIVATE_SUBNET_AVAILABILITY_ZONE
  cidr_block              = var.PRIVATE_SUBNET_CIDR_BLOCK
  map_public_ip_on_launch = "false"
  tags = {
    Name = var.PRIVATE_SUBNET_TAG_NAME
  }
}
