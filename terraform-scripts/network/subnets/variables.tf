variable "VPC_ID" {
  description = "VPC ID for where subnets have to be created"
}

variable "PRIVATE_SUBNET_AVAILABILITY_ZONE" {
  description = "Private Subnet Availability Zone"
}

variable "PRIVATE_SUBNET_CIDR_BLOCK" {
  description = "Private Subnet CIDR Range"
}

variable "PRIVATE_SUBNET_TAG_NAME" {
  description = "Private Subnet Tag Name"
}

variable "PUBLIC_SUBNET_AVAILABILITY_ZONE" {
  description = "Public Subnet Availability Zone"
}

variable "PUBLIC_SUBNET_CIDR_BLOCK" {
  description = "Public Subnet CIDR Range"
}

variable "PUBLIC_SUBNET_TAG_NAME" {
  description = "Public Subnet Tag Name"
}
