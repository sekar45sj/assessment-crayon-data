variable "PUBLIC_SUBNET_ID" {
  description = "Public Subnet ID for where to create NAT Gateway"
}

variable "NAT_GATEWAY_TAG_NAME" {
  description = "NAT Gateway Tag Name"
}
