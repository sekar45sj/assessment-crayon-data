/*
    Elastic IP Creation for NAT Gateway
*/
resource "aws_eip" "eip" {
  vpc = true
}

/*
    NAT Gateway Creation in Public Subnet
*/
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.eip.id
  subnet_id     = var.PUBLIC_SUBNET_ID
  tags = {
    Name = var.NAT_GATEWAY_TAG_NAME
  }
}
