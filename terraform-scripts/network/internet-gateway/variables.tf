variable "VPC_ID" {
  description = "VPC ID for attaching Internet Gateway"
}

variable "INTERNET_GATEWAY_TAG_NAME" {
  description = "Internet Gateway Tag Name"
}
