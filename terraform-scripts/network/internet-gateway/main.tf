/*
    Internet Gateway Creation in specified VPC
*/
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = var.VPC_ID
  tags = {
    Name = var.INTERNET_GATEWAY_TAG_NAME
  }
}
